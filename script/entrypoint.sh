#!/bin/sh

#Script to be run inside the docker image - most of the steps are specific to a Django application
###########################################

#Exit the script immediatly if an error is found
set -e 

#Allows you to collect all of the static files required for your project and store them in a single diretory
python manage.py collectstatic --noinput

#Migration file to make necessary changes to the database, such as adding / removing a table
#A - confirm the database is running, holds the database until it is accessible
python manage.py wait_for_db

#B - run the migration on our database
python manage.py migrate 

#uwsgi is the name of the application
#run the application on port 9000
#number of workers is set
#master flag, run it as a master service on the terminal (if Docker closes, it will ensure it will gracefull closes)
#module is the application the uwsgi will run
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi

data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

#   tags = merge(
#     local.common_tags,
#     map("Name", "${local.prefix}-bastion")
#   )
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )

}


//Add resource in Terraform - data is to retrieve information from AWS
//Data source = to get data from a reource
//Resource = to create a resource in AWS


terraform {

  #Configure terraform to store the state in S3 (you can store it locally too)
  #The key parameter acts as a path
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-hugo"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
      Environment = terraform.workspace 
      Project = var.project
      Owner = var.contact
      ManagedBy = "Terraform"
  }
}
